#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import numpy as np
import string
import nltk
nltk.download('stopwords', quiet=True)
nltk.download('punkt', quiet=True)
nltk.download('averaged_perceptron_tagger', quiet=True)
nltk.download('wordnet', quiet=True)
nltk.download('vader_lexicon', quiet=True)
nltk.download('omw-1.4', quiet=True)
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from gensim import corpora, models
import matplotlib.pyplot as plt
from wordcloud import WordCloud


# In[ ]:


# word frequency analysis
def plot_top_lemmas(df, continent):
    '''
    Plots a wordcloud of the top 15 lemmas in lyrics from each continent.
    '''
    lemmas = df.apply(get_lemmas)
    
    # Extend list so that it contains all words/parts of speech for all the captions
    lemmas_full = []
    for j in lemmas:
        lemmas_full.extend(j)
        
    # Convert list to string because word cloud only takes string input
    text = ' '.join(map(str,lemmas_full))
    
    # Generate a word cloud image
    wordcloud = WordCloud().generate(text)
    # Display the generated image:
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.suptitle(f'Most Frequenctly Occurring Lemmas for National Anthem Lyrics of {continent}')
    plt.title('Bigger size means higher frequency')
    plt.axis("off")
    plt.show()
    
    return

def plot_top_tfidf(df, continent):
    '''
    Plots the top 15 TF-IDF words in a Pandas series of strings.
    Average TF-IDF score for each word is computed across all documents.
    '''
    # Get lemmas for each row in the input Series
    lemmas = df.apply(get_lemmas)

    # Initialize Series of lemmas as Gensim Dictionary for further processing
    dictionary = corpora.Dictionary([i for i in lemmas])
    
    # Convert dictionary into bag of words format: list of (token_id, token_count) tuples
    bow_corpus = [dictionary.doc2bow(text) for text in lemmas] 

    # Calculate TFIDF based on bag of words counts for each token and return weights:
    tfidf = models.TfidfModel(bow_corpus, normalize=True)
    tfidf_weights = {}
    for doc in tfidf[bow_corpus]:
        for ID, freq in doc:
            tfidf_weights[dictionary[ID]] = np.around(freq, decimals = 2) + tfidf_weights.get(dictionary[ID], 0)

    # highest TF-IDF values:
    top_n = pd.Series(tfidf_weights).nlargest(15) / len(lemmas)
    
    # Plot the top n weighted words:
    fig, ax = plt.subplots() # to plot separate histograms instead stacking in one
    ax.plot(top_n.index, top_n.values)
    plt.xticks(rotation='vertical')
    plt.xlabel('Words')
    plt.ylabel('TFIDF Values')
    plt.title(f'Top 15 Lemmas (TFIDF) for National Anthem Lyrics of {continent}');

    return


# In[ ]:


# sentiment analysis
sid = SentimentIntensityAnalyzer()
def sentiment_score(text):
    return sid.polarity_scores(text)['compound']

def plot_sentiment(df, continent):
    fig, ax = plt.subplots() # to plot separate histograms instead stacking in one
    ax.hist(df.apply(sentiment_score))
    plt.xlabel('Sentiment Score')
    plt.ylabel('Count')
    plt.title(f'Sentiment Scores for National Anthem Lyrics of {continent}')

