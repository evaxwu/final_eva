#!/usr/bin/env python
# coding: utf-8

# In[2]:


import pandas as pd
import numpy as np
import string
import nltk
nltk.download('stopwords', quiet=True)
nltk.download('punkt', quiet=True)
nltk.download('averaged_perceptron_tagger', quiet=True)
nltk.download('wordnet', quiet=True)
nltk.download('vader_lexicon', quiet=True)
nltk.download('omw-1.4', quiet=True)
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from gensim import corpora, models


# In[ ]:


# Define stop words + punctuation + study-specific stop-words
STOP = nltk.corpus.stopwords.words('english') + list(string.punctuation)


# In[ ]:


def pos_tag(text):
    '''
    Tags each word in a string with its part-of-speech indicator, excluding stop-words
    '''
    # Tokenize words using nltk.word_tokenize, keeping only those tokens that do not appear in the stop words we defined
    tokens = [i for i in nltk.word_tokenize(text.lower()) if i not in STOP]

    # Label parts of speech automatically using NLTK
    pos_tagged = nltk.pos_tag(tokens)
    return pos_tagged


# In[ ]:


def get_wordnet_pos(word):
    '''
    Tags each word with its Part-of-speech indicator - specifically used for lemmatization in the get_lemmas function
    '''
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": nltk.corpus.wordnet.ADJ,
                "N": nltk.corpus.wordnet.NOUN,
                "V": nltk.corpus.wordnet.VERB,
                "R": nltk.corpus.wordnet.ADV}
    return tag_dict.get(tag, nltk.corpus.wordnet.NOUN)


# In[ ]:


def get_lemmas(text):
    '''
    Gets lemmas for a string input, excluding stop words, punctuation, as well as a set of study-specific stop-words
    '''
    tokens = [i for i in nltk.word_tokenize(text.lower()) if i not in STOP]
    lemmas = [nltk.stem.WordNetLemmatizer().lemmatize(t, get_wordnet_pos(t)) for t in tokens]
    return lemmas

