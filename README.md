# Final Project #

Goal: apply what I have learned in class to my own work.

Prompt:

1. Design or improve your analysis pipeline.

2. Create a repo.

3. Incorporate modularity in your code. Create structure in your data storage.

4. Explore the data, visually or otherwise.

5. Identify some interesting features or relationships.

6. Communicate your journey to the class.