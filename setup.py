import setuptools

setuptools.setup(
    name="explore_wordcloud",
    version="0.1.0",
    author="Eva Wu",
    author_email="wux21@uchicago.edu",
    description="Final Project for PSYC 40650",
    url="https://bitbucket.org/evaxwu/final_eva/",
    packages=setuptools.find_packages(),
    install_requires=[
        'pandas', 'nltk', 'gensim', 'matplotlib', 'wordcloud',
        'numpy', 'scipy', 'scikit-learn', 'seaborn'
    ],
)